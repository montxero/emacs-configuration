;;; package --- Summary
;;; Commentary: Packages I use.
;;; Code:
(defvar *my-packages*
  '(ac-slime
    ample-theme
    auctex
    auto-complete
    auto-complete-auctex
    clang-format
    clang-format+
    company
    company-tern
    elpy
    flatland-theme
    flycheck
    flymake-shellcheck
    geiser
    gnu-elpa-keyring-update
    gnuplot
    gnuplot-mode
    hamburg-theme
    haskell-mode
    helm
    helm-gtags
    htmlize
    inf-ruby
    jedi
    js2-mode
    js2-refactor
    magit
    markdown-mode
    monokai-theme
    ob-sagemath
    org
    ox-reveal
    paredit
    pipenv
    prettier-js
    projectile
    protobuf-mode
    py-autopep8
    python-black
    python-pytest
    quack
    racket-mode
    reftex
    sage-shell-mode
    shen-mode
    sicp
    slime
    sml-mode
    spacemacs-theme
    spacemacs-theme
    sqlup-mode
    tern
    vimish-fold
    web-mode
    xref-js2
    yasnippet
    yasnippet-snippets))
(provide '*my-packages*)
;;; my-packages.el ends here.
